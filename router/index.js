const express = require('express');
const router = express.Router();
router.use(express.json());
const middleware = require('../middleware/auth');
const userController = require('../controller/users');

router.get('/users', userController.getAllUsers);
router.get('/users/:id', middleware, userController.getUserById);
router.post('/users', middleware, userController.createUser);
router.put('/users/:id', middleware, userController.updateUser);
router.delete('/users/:id', middleware, userController.deleteUser);


router.get('/db/users', userController.getAllUsersFromDB);
router.get('/db/users/:id', userController.getUserByIdFromDB);
router.post('/db/users', userController.createUserFromDB);
router.put('/db/users/:id', userController.updateUserFromDB);
router.delete('/db/users/:id', userController.deleteUserFromDB);

module.exports = router;