const config = require('./config');
const mongoose = require('mongoose');


function connectDatabase(){
   return mongoose.connect(config.URL, { useNewUrlParser: true });
}

module.exports = connectDatabase;