const express = require('express');
const router = express.Router();

router.use((err, req, res, next)=>{
    res.json({status: false, message: err.message});
});

module.exports = router;