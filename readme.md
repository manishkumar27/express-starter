# Express-Starter
This README outlines the details of understanding and running up this application.

## Prerequisites
Things you may need to install while testing/running this application  
 * Git  
 * Node.js  
### Dependencies
**express**: ^4.16.3  
**mongoose**: ^5.2.8  

## Installation
* `git clone https://manishkumar27@bitbucket.org/manishkumar27/express-starter.git`
* `node server.js`

## Get In Touch
Contact me on manish.k1@successivesoftwares.com, for queries and fixes.
