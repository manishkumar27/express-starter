const database = require('../data/userModel');
const userModelMongoDB = require('../data/userModelSchema');

const middleware = ((req, res, next) => {

    if (req.method == 'POST') {
        database.isUserExist(req.body.id)
            ? next(new Error("User Already Exist, Invalid ID"))
            : next();
    }
    else {
        database.isUserExist(req.params.id)
            ? next()
            : next(new Error("User not found, Invalid ID"));
    }
});

module.exports = middleware;