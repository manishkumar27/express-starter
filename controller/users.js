const database = require('../data/userModel');
const userModelMongoDB = require('../data/userModelSchema');

class UserController {

    getAllUsers(req, res, next) {
        let usersList = database.getAllUsersFromDB();
        res.json(usersList);
    }
    getUserById(req, res, next) {
        let user = database.getUserByIdFromDB(req.params.id);
        res.json(user);
    }
    createUser(req, res, next) {
        database.createUserInDB(req.body);
        res.json({ status: true, message: "User added successfully" });
    }
    updateUser(req, res, next) {
        database.updateUserInDB(req.params.id, req.body);
        res.json({ status: true, message: "User updated successfully" });
    }
    deleteUser(req, res, next) {
        database.deleteUserInDB(req.params.id)
        res.json({ status: true, message: "User deleted successfully" });
    }

    //--------------------------------------------------------------------------
    //----------------------- BELOW METHODS ARE FOR MONGO DB -------------------

    getAllUsersFromDB(req, res, next) {
        userModelMongoDB.getAllUsers()
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }
    getUserByIdFromDB(req, res, next) {
        userModelMongoDB.getUserByID(req.params.id)
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }
    createUserFromDB(req, res, next) {
        userModelMongoDB.createUser(req.body)
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }
    updateUserFromDB(req, res, next) {
        userModelMongoDB.updateUser(req.params.id, req.body)
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }
    deleteUserFromDB(req, res, next) {
        userModelMongoDB.deleteUser(req.params.id)
            .then((doc) => res.json(doc))
            .catch((err) => res.json(err));
    }


}

module.exports = new UserController();