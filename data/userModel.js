const express = require('express');

class UserModel{

    constructor(){
        this.database = [
                            {
                            "id": 1,
                            "first_name": "Karol",
                            "last_name": "Topham",
                            "email": "ktopham0@yolasite.com"
                            },
                            {
                            "id": 2,
                            "first_name": "Jere",
                            "last_name": "Deakin",
                            "email": "jdeakin1@seattletimes.com"
                            },
                            {
                            "id": 3,
                            "first_name": "Gasper",
                            "last_name": "Gaucher",
                            "email": "ggaucher2@zimbio.com"
                            },
                            {
                            "id": 4,
                            "first_name": "Bernardo",
                            "last_name": "Crinage",
                            "email": "bcrinage3@mail.ru"
                            },
                            {
                            "id": 5,
                            "first_name": "Evyn",
                            "last_name": "Hirtz",
                            "email": "ehirtz4@technorati.com"
                            },
                            {
                            "id": 6,
                            "first_name": "Benoite",
                            "last_name": "Male",
                            "email": "bmale5@guardian.co.uk"
                            },
                            {
                            "id": 7,
                            "first_name": "Adrian",
                            "last_name": "Connar",
                            "email": "aconnar6@homestead.com"
                            },
                            {
                            "id": 8,
                            "first_name": "Manish",
                            "last_name": "Sundriyal",
                            "email": "manish@sundriyal.com"
                            },
                            {
                            "id": 9,
                            "first_name": "Mansi",
                            "last_name": "Goswami",
                            "email": "mansi@gmail.com"
                            }
                        ];
    }

    isUserExist(id){
        for(let i=0;i<this.database.length;++i){
            if(this.database[i].id==id){
                return true;
            }
        }
        return false;
    }

    getAllUsersFromDB(){
        return this.database;
    }

    getUserByIdFromDB(id){
        for(let i=0;i<this.database.length;++i){
            if(this.database[i].id==id){
                return this.database[i];
            }
        }
    }

    createUserInDB(dataObj){
        this.database.push(dataObj);
    }

    updateUserInDB(id, dataObj){
        for(let i=0;i<this.database.length;++i){
            if(this.database[i].id==id){
                this.database[i] = Object.assign({},dataObj);
                console.log(dataObj);
                break;
            }
        }
    }
    deleteUserInDB(id){
        for(let i=0;i<this.database.length;++i){
            if(this.database[i].id==id){
                this.database.splice(i,1);
                break;
            }
        }
    }

}
module.exports = new UserModel();