const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    id: { type: Number, require: true, trim: true, unique: true },
    firstName: { type: String, require: true, trim: true },
    lastName: { type: String, require: true, trim: true },
    email: { type: String, require: true, trim: true }
});

mongoose.connection.on('error', (err) => console.log(err));
mongoose.connection.on('disconnected', () => console.log("database diconnected"));
mongoose.connection.on('connected', () => console.log("database connected"));

/**
 * fetchs all the documents
 * @returns {Promise}
 */
UserSchema.statics.getAllUsers = function () {
    return this.find();
}
/**
 * fetchs one document of matching id
 * @param {Number} dataID id
 * @returns {Promise}
 */
UserSchema.statics.getUserByID = function (dataID) {
    return this.findOne({ id: dataID });
}
/**
 * adds one document in the collection
 * @param {Object} dataObject (id, firstName, lastName, email) 
 * @returns {Promise}
 */
UserSchema.statics.createUser = function (dataObject) {
    return new this(dataObject).save();
}
/**
 * updates one document in the collection of matching id
 * @param {Number} dataID id
 * @param {Object} dataObject (id, firstName, lastName, email) 
 * @returns {Promise}
 */
UserSchema.statics.updateUser = function (dataID, dataObject) {
    return this.findOneAndUpdate({ id: dataID }, { $set: dataObject })
}
/**
 * remove on doument from the collection of matching id
 * @param {Number} dataID id
 * @returns {Promise}
 */
UserSchema.statics.deleteUser = function (dataID) {
    return this.findOneAndRemove({ id: dataID });
}

module.exports = mongoose.model('User', UserSchema);