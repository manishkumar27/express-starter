const express = require('express');
const app = express();
const config = require('./config');
const router = require('./router/index');
app.use(express.json());
const errorHandler = require('./helper/errorHandler');
const connectDatabase = require('./db');


app.use('/', router);

connectDatabase()
    .then((res) => {
        app.get('/', (req, res) => {
            res.send("Welcome Page");
        }).listen(config.PORT);

    }).catch((err) => console.log("Unable to connect to database"));

// app.use(errorHandler);
app.use((err, req, res, next) => {
    res.json({ status: false, message: err.message });
});

